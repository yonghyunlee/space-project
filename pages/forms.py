from django import forms
# from pages.models import Story


class StoryForm(forms.Form):
    sender = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'true'}),
    )

    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'true'}),
    )

    story = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'required': 'true'}),
    )


class CommentForm(forms.Form):
    commentWindow = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'readonly': 'true'}),
    )
    comment = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
    )
# class StoryForm(forms.ModelForm):
#     class Meta:
#         model = Story
#         fields = ['sender', 'title', 'story']
#         widgets = {
#             'sender': forms.CharField(
#                 widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'true'}),
#             ),
#             'title': forms.CharField(
#                 widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'true'}),
#             ),
#             'story': forms.Textarea(attrs={'class': 'form-control', 'required': 'true'}),
#         }

