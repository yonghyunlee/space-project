from django.urls import path

from .views import HomePageView, AboutPageView, SendStoryPageView, CommentPageView, StoryReadView, MainPageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('main/', MainPageView.as_view(), name='main'),
    path('about/', AboutPageView.as_view(), name='about'),
    path('story/', SendStoryPageView.as_view(), name='story'),
    path('comment/', CommentPageView.as_view(), name='comment'),
    path('storyAudio/', StoryReadView.as_view(), name='storyAudio'),
]
