import os

from django.conf import settings
from django.views.generic import TemplateView, FormView, DetailView
from django.views.generic.base import ContextMixin
from gtts import gTTS

from pages.forms import StoryForm, CommentForm

# storyIndex = len(os.listdir(settings.BASE_DIR + '/static/TTS')) - 1


def get_story():
    path = settings.BASE_DIR
    story_list = os.listdir(path + '/static/TTS')

    if len(story_list) == 0:
        return '사연 없음.'
    story = story_list[0]
    story = story.split('.')[0]
    return story


class HomePageView(TemplateView, ContextMixin):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        story = get_story()
        context['story'] = story
        return context


class MainPageView(TemplateView, ContextMixin):
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        story = get_story()
        context['story'] = story
        return context


class AboutPageView(TemplateView):
    template_name = 'pages/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        story = get_story()
        context['story'] = story
        return context


class SendStoryPageView(FormView, ContextMixin):
    template_name = 'pages/sendStory.html'
    form_class = StoryForm
    success_url = '/main'

    def form_valid(self, form):
        sender = self.request.POST['sender']
        title = self.request.POST['title']
        story = self.request.POST['story']
        tts = sender + '님의 사연입니다. ' + title + '. ' + story
        tests = gTTS(text=tts, lang='ko')
        tests.save("%s.mp3" % os.path.join('./static/TTS/', sender+'-'+title))
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        story = get_story()
        context['story'] = story
        return context


class CommentPageView(FormView, ContextMixin):
    template_name = 'pages/comment.html'
    form_class = CommentForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        story = get_story()
        context['story'] = story
        return context


class StoryReadView(TemplateView, ContextMixin):
    template_name = 'story/storyAudio.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        path = settings.BASE_DIR
        story_list = os.listdir(path + '/static/TTS')
        story = story_list[0]
        context['story'] = 'TTS/'+story
        return context

