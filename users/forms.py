from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    username = forms.CharField(max_length=CustomUser._meta.get_field('username').max_length)

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = ('email', 'username', 'password')


class CustomUserChangeForm(UserChangeForm):
    username = forms.CharField(max_length=CustomUser._meta.get_field('username').max_length)

    class Meta:
        model = CustomUser
        fields = ('email', 'username', 'password')
